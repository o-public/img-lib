import io
import logging
import os.path
import zipfile
from zipfile import ZipFile

import boto3

from s3_image_handler.config import BOTO_CLIENT_CONFIG

logger = logging.getLogger('download_req')


def download_and_unzip(bucket_name, object_name, save_to, extract_to, delete_zip=True):
    logger.info("downloading files from bucket: {} file: {} ".format(bucket_name, object_name))
    s3 = boto3.client('s3', config=BOTO_CLIENT_CONFIG)
    downloaded_file = os.path.join(save_to, 'downloaded_req.zip')
    s3.download_file(bucket_name, object_name, downloaded_file)
    logger.info("extracting files from bucket: {} file: {} ".format(bucket_name, object_name))
    zipfile = ZipFile(downloaded_file)
    zipfile.extractall(path=extract_to)
    logger.info("extracting finished")
    if delete_zip:
        os.remove(downloaded_file)


def zip_pil_images(pil_images):
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "a", zipfile.ZIP_DEFLATED, False) as zip_file:
        for idx, img in enumerate(pil_images):
            img_fp = io.BytesIO()
            img.save(img_fp, format='JPEG')
            zip_file.writestr(str(idx).zfill(5) + ".jpg", img_fp.getvalue())

    return zip_buffer


def zip_images(images):
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "a", zipfile.ZIP_DEFLATED, False) as zip_file:
        for idx, img in enumerate(images):
            zip_file.writestr(str(idx).zfill(5) + ".jpg", img.getvalue())

    return zip_buffer
