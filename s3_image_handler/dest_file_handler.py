import os.path
from io import BytesIO
from pathlib import Path

import boto3
import numpy as np

from s3_image_handler.config import BOTO_CLIENT_CONFIG


class DestFileHandler:
    def __init__(self,
                 bucket_name,
                 local_dest_folder,
                 prefix='DEST',
                 org_suffix='ORG',
                 aligned_suffix='ALIGNED',
                 latent_suffix='LATENT',
                 get_url_expire=3600):
        self.bucket_name = bucket_name
        self.org_suffix = org_suffix
        self.aligned_suffix = aligned_suffix
        self.latent_suffix = latent_suffix
        self.delimiter = "_"
        self.remote_delimiter = "/"
        self.prefix = prefix
        self.get_url_expire = get_url_expire
        self.local_dest_folder = local_dest_folder
        Path(self.local_dest_folder).mkdir(parents=True, exist_ok=True)
        self.client = boto3.client('s3', config=BOTO_CLIENT_CONFIG)

    def upload_latent(self, category, image_id, latent_arr):
        np.save(self.latent_file(category, image_id), latent_arr)

    def get_latent(self, category, image_id):
        if not os.path.exists(self.latent_file(category, image_id)):
            return None
        return np.load(self.latent_file(category, image_id))

    def latent_file(self, category, image_id):
        return os.path.join(self.local_dest_folder, self.latent_key(category, image_id) + ".npy")

    def latent_key(self, category, image_id):
        return self.delimiter.join([self.prefix, category.lower(), image_id, self.latent_suffix])

    def org_key(self, category, image_id):
        return self.delimiter.join([self.prefix, category.lower(), image_id, self.org_suffix])

    def aligned_key(self, category, image_id):
        return self.delimiter.join([self.prefix, category.lower(), image_id, self.aligned_suffix])

    def org_file(self, category, image_id):
        return os.path.join(self.local_dest_folder, self.org_key(category, image_id))

    def aligned_file(self, category, image_id):
        return os.path.join(self.local_dest_folder, self.aligned_key(category, image_id) + ".npy")

    def get_org(self, category, image_id):
        if not os.path.exists(self.org_file(category, image_id)):
            return None
        with open(self.org_file(category, image_id), 'rb') as f:
            output = f.read()

        return output

    def get_aligned(self, category, image_id):
        if not os.path.exists(self.aligned_file(category, image_id)):
            return None
        return np.load(self.aligned_file(category, image_id))

    def upload_org(self, category, image_id, img_bytes):
        with open(self.org_file(category, image_id), "wb") as org_file:
            org_file.write(img_bytes)

    def upload_aligned(self, category, image_id, img_bytes):
        np.save(self.aligned_file(category, image_id), img_bytes)

    def get_org_remote(self, category, image_id):
        res = self.client.get_object(Bucket=self.bucket_name, Key=self.remote_org_key(category, image_id))
        obj_bytes = res['Body'].read()
        return obj_bytes

    def remote_org_key(self, category, image_id):
        return self.remote_delimiter.join([self.prefix, category.lower(), image_id, self.org_suffix])

    def upload_org_remote(self, category, image_id, img_bytes):
        self.client.upload_fileobj(BytesIO(img_bytes), self.bucket_name, self.remote_org_key(category, image_id))

    def get_org_with_remote(self, category, image_id):
        result = self.get_org(category, image_id)
        if result:
            return result

        result = self.get_org_remote(category, image_id)
        if not result:
            return None

        self.upload_org(category, image_id, result)
        return result

    def list_remote_org_files(self, category):

        def create_prefix(cat):
            return self.prefix + self.remote_delimiter + cat

        return self.client.list_objects_v2(Bucket=self.bucket_name, Prefix=create_prefix(category))

    def generate_presigned_url_and_id(self, id):
        public_id = id.split(self.remote_delimiter)[2]

        url = self.client.generate_presigned_url('get_object',
                                                 Params={'Bucket': self.bucket_name,
                                                         'Key': id,
                                                         'ResponseContentDisposition': 'attachment; filename ="' + public_id + '"'
                                                         },
                                                 ExpiresIn=self.get_url_expire)
        return {
            "id": public_id,
            "url": url
        }

    def get_remote_files_presigned(self, category):
        files = self.list_remote_org_files(category)

        def filter_image(id):
            return id.startswith(self.prefix + self.remote_delimiter + category) and id.endswith(self.org_suffix)

        return list(map(self.generate_presigned_url_and_id,
                        filter(filter_image, map(lambda content: content['Key'], files['Contents']))))
