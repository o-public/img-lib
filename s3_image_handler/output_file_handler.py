import io

import boto3

from s3_image_handler.config import BOTO_CLIENT_CONFIG


class OutputFileHandler:
    def __init__(self, bucket_name, prefix='OUT', get_url_expire=10800):
        self.bucket_name = bucket_name
        self.delimiter = "/"
        self.prefix = prefix
        self.get_url_expire = get_url_expire
        self.client = boto3.client('s3', config=BOTO_CLIENT_CONFIG)

    def get_output(self, img_id):
        res = self.client.get_object(Bucket=self.bucket_name, Key=self.output_key(img_id))
        return res['Body'].read()

    def upload_output(self, img_bytes, img_id):
        self.client.upload_fileobj(io.BytesIO(img_bytes), self.bucket_name, self.output_key(img_id))

    def upload_output_zip(self, zip_bytes, zip_id):
        self.client.upload_fileobj(io.BytesIO(zip_bytes), self.bucket_name, self.output_zip_key(zip_id))

    def get_output_zip(self, zip_id):
        res = self.client.get_object(Bucket=self.bucket_name, Key=self.output_zip_key(zip_id))
        return res['Body'].read()

    def output_zip_key(self, zip_id):
        return self.output_key(zip_id) + ".zip"

    def output_key(self, img_id):
        return self.prefix + self.delimiter + img_id

    def head_output_zip(self, image_id):
        return self.client.head_object(Bucket=self.bucket_name, Key=self.output_zip_key(image_id))

    def generate_presigned_url(self, file_id):
        return self.client.generate_presigned_url('get_object', Params={'Bucket': self.bucket_name,
                                                                        'Key': self.output_zip_key(file_id)},
                                                  ExpiresIn=self.get_url_expire)
