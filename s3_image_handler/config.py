from botocore.config import Config

BOTO_CLIENT_CONFIG = Config(signature_version='s3v4', connect_timeout=7, retries={'max_attempts': 3})
