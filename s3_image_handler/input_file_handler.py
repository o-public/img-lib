import pickle
from io import BytesIO

import boto3

from s3_image_handler.config import BOTO_CLIENT_CONFIG


class InputFileHandler:
    def __init__(self,
                 bucket_name,
                 prefix="SRC",
                 org_suffix="ORG",
                 aligned_suffix="ALIGNED",
                 latent_suffix="LATENT",
                 post_url_expire=3600):
        self.bucket_name = bucket_name
        self.org_suffix = org_suffix
        self.aligned_suffix = aligned_suffix
        self.latent_suffix = latent_suffix
        self.delimiter = "/"
        self.prefix = prefix
        self.post_url_expire = post_url_expire
        self.client = boto3.client('s3', config=BOTO_CLIENT_CONFIG)

    def check_bucket_exits(self):
        return self.client.head_bucket(Bucket=self.bucket_name)

    def upload_latent(self, image_id, latent_arr):
        self.client.upload_fileobj(BytesIO(latent_arr.dumps()), self.bucket_name, self.latent_key(image_id))

    def head_latent(self, image_id):
        return self.client.head_object(Bucket=self.bucket_name, Key=self.latent_key(image_id))

    def get_latent(self, image_id):
        res = self.client.get_object(Bucket=self.bucket_name, Key=self.latent_key(image_id))
        obj_bytes = res['Body'].read()
        return pickle.loads(obj_bytes)

    def latent_key(self, image_id):
        return self.delimiter.join([self.prefix, image_id, self.latent_suffix])

    def head_aligned(self, image_id):
        return self.client.head_object(Bucket=self.bucket_name, Key=self.aligned_key(image_id))

    def get_aligned(self, image_id):
        res = self.client.get_object(Bucket=self.bucket_name, Key=self.aligned_key(image_id))
        obj_bytes = res['Body'].read()
        return pickle.loads(obj_bytes)

    def upload_aligned(self, image_id, aligned_arr):
        self.client.upload_fileobj(BytesIO(aligned_arr.dumps()), self.bucket_name, self.aligned_key(image_id))

    def aligned_key(self, image_id):
        return self.delimiter.join([self.prefix, image_id, self.aligned_suffix])

    def get_objects(self):
        return self.client.list_objects_v2(Bucket=self.bucket_name,
                                           Prefix=self.prefix + self.delimiter,
                                           Delimiter=self.delimiter, MaxKeys=20)

    def org_key(self, image_id):
        return self.delimiter.join([self.prefix, image_id, self.org_suffix])

    def get_org(self, image_id):
        res = self.client.get_object(Bucket=self.bucket_name, Key=self.org_key(image_id))
        obj_bytes = res['Body'].read()
        return obj_bytes

    def upload_org(self, image_id, img_bytes):
        self.client.upload_fileobj(BytesIO(img_bytes), self.bucket_name, self.org_key(image_id))

    def head_org(self, image_id):
        return self.client.head_object(Bucket=self.bucket_name, Key=self.org_key(image_id))

    def get_org_image_id(self, file_name):
        parsed = file_name.split('/')
        return parsed[1]

    def generate_signed_url_for_upload(self, image_id, conditions=None):
        if conditions is None:
            conditions = [["content-length-range", 1, 3495253]]
            
        return self.client.generate_presigned_post(Bucket=self.bucket_name, Key=self.org_key(image_id),
                                                   ExpiresIn=self.post_url_expire,
                                                   Conditions=conditions)

    def generate_signed_url_for_put_upload(self, image_id):
        return self.client.generate_presigned_url('put_object', Params={'Bucket': self.bucket_name,
                                                                        'Key': self.org_key(image_id)},
                                                  ExpiresIn=self.post_url_expire)
