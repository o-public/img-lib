import io
import pickle

import boto3

from s3_image_handler.config import BOTO_CLIENT_CONFIG


class S3FileBucket:
    def __init__(self, bucket_name, base_path=None, folder_delimiter="/"):
        self.client = boto3.client('s3', config=BOTO_CLIENT_CONFIG)
        self.bucket_name = bucket_name
        self.base_path = base_path
        self.folder_delimiter = folder_delimiter

    def file_key(self, file_name, path=None):
        tmp_path = path if path is not None else self.base_path
        if tmp_path:
            return self.folder_delimiter.join([tmp_path, file_name])
        else:
            return self.folder_delimiter.join([file_name])

    def get_file_bytes_remote(self, file_name, path=None):
        res = self.client.get_object(Bucket=self.bucket_name, Key=self.file_key(file_name, path))
        return res['Body'].read()

    def save_file_bytes_remote(self, bytes_to_save, file_name, path=None):
        self.client.upload_fileobj(io.BytesIO(bytes_to_save), self.bucket_name, self.file_key(file_name, path))

    def get_file_remote(self, file_name, path=None):
        file_bytes = self.get_file_bytes_remote(file_name, path)
        return pickle.loads(file_bytes)

    def save_file_remote(self, object_to_upload, file_name, path=None):
        outp = io.BytesIO()
        pickle.dump(object_to_upload, outp, pickle.HIGHEST_PROTOCOL)
        self.save_file_bytes_remote(outp.getvalue(), file_name, path)
